import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

  constructor(props){
    super(props);
    this.state={
      num:0
    }
    this.plusOne=this.plusOne.bind(this);
    this.reduction=this.reduction.bind(this);
    this.multiplicationTwo=this.multiplicationTwo.bind(this);
    this.divisionTwo=this.divisionTwo.bind(this);
  }
  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result" > {this.state.num} </span>
        </div>
        <div className="operations">
          <button onClick={this.plusOne}>加1</button>
          <button onClick={this.reduction}>减1</button>
          <button onClick={this.multiplicationTwo}>乘以2</button>
          <button onClick={this.divisionTwo}>除以2</button>
        </div>
      </section>
    );
  }

  plusOne(){
  this.setState({
    num:this.state.num+1
  })
}

  reduction(){
    this.setState({
      num:this.state.num-1
    })
  }

  multiplicationTwo(){
    this.setState({
      num:this.state.num*2
    })
  }

  divisionTwo(){
    this.setState({
      num:this.state.num/2
    })
  }


}

export default MiniCalculator;

